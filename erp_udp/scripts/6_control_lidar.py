
from morai_udp_parser import erp_udp_parser,erp_udp_sender
from serial_node import erpSerial
from utils import pathReader,findLocalPath,purePursuit,Point
import time
import threading
from math import cos,sin,sqrt,pow,atan2,pi
import os
import matplotlib.pyplot as plt
from lidar_util import UDP_LIDAR_Parser
import numpy as np
user_ip='127.0.0.1'

params_lidar = {
    "Range" : 90, #min & max range of lidar azimuths
    "CHANNEL" : 16, #verticla channel of a lidar
    "localIP": "127.0.0.1",
    "localPort": 2368,
    "Block_SIZE": int(1206)
}


class planner :

    def __init__(self):
        self.status=erp_udp_parser(user_ip, 7800,'status')
        self.obj=erp_udp_parser(user_ip, 7700,'obj')
        self.ctrl_cmd=erp_udp_sender(user_ip,7601)
        self.udp_lidar = UDP_LIDAR_Parser(ip=params_lidar["localIP"], port=params_lidar["localPort"], params_lidar=params_lidar)

        self.txt_reader=pathReader()
        self.global_path=self.txt_reader.read('kcity.txt')

        self.pure_pursuit=purePursuit()
  

        self._is_status=False
        while not self._is_status :
            if not self.status.get_data() :
                print('No Status Data Cannot run main_loop')
                time.sleep(1)
            else :
                self._is_status=True


        self.main_loop()


    
    def main_loop(self):
        self.timer=threading.Timer(0.33,self.main_loop)
        self.timer.start()
        
        status_data=self.status.get_data()
        obj_data=self.obj.get_data()
        position_x=status_data[0]
        position_y=status_data[1]
        position_z=status_data[2]
        heading=status_data[5]+90   # degree
        velocity=status_data[6]

        local_path,current_point =findLocalPath(self.global_path,position_x,position_y)
        self.pure_pursuit.getPath(local_path)
        self.pure_pursuit.getEgoStatus(position_x,position_y,position_z,velocity,heading)




        if self.udp_lidar.is_lidar ==True:
            x=self.udp_lidar.x
            y=self.udp_lidar.y
            z=self.udp_lidar.z
            intensity=self.udp_lidar.Intensity 

            xyz1 = np.concatenate([
                x.reshape([-1, 1]),
                y.reshape([-1, 1]),
                z.reshape([-1, 1])
            ], axis=1).T.astype(np.float32)

            print(xyz1.T)


    

        steering_angle=self.pure_pursuit.steering_angle()
        accel=1
        brake=0
        self.ctrl_cmd.send_data(accel,brake,steering_angle)
        
       
        
        



        # print('--------------------status-------------------------')
        # print('position :{0} ,{1}, {2}'.format(position_x,position_y,position_z))
        # print('velocity :{} km/h'.format(velocity,heading))
        # print('heading :{} deg'.format(heading))

        # print('--------------------object-------------------------')
        # print('object num :{}'.format(len(obj_data)))
        # for i,obj_info in enumerate(obj_data) :
        #     print('{0} : type = {1}, x = {2}, y = {3}, z = {4} '.format(i,obj_info[0],obj_info[1],obj_info[2],obj_info[3]))

        # print('--------------------controller-------------------------')
        # print('target steering_angle :{} deg'.format(steering_angle))

        # print('--------------------localization-------------------------')
        # print('all waypoint size: {} '.format(len(self.global_path)))
        # print('current waypoint : {} '.format(current_point))



if __name__ == "__main__":


    kicty=planner()
    while True :
        pass
 








