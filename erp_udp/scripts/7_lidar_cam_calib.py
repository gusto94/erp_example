import socket
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from common_util import RotationMatrix, TranslationMatrix
from cam_util import UDP_CAM_Parser
from lidar_util import UDP_LIDAR_Parser, LIDAR2CAMTransform, make_intens_img


params_cam = {
    "WIDTH": 480, # image width
    "HEIGHT": 320, # image height
    "FOV": 90, # Field of view
    "localIP": "127.0.0.1",
    "localPort": 1232,
    "Block_SIZE": int(65000),
    "X": 0.62, # meter
    "Y": 0.0,
    "Z": 0.57,
    "YAW": 0.0, # deg
    "PITCH": 0.0,
    "ROLL": 0.0
}


params_lidar = {
    "Range" : 90, #min & max range of lidar azimuths
    "CHANNEL" : 16, #verticla channel of a lidar
    "localIP": "127.0.0.1",
    "localPort": 2368,
    "Block_SIZE": int(1206),
    "X": 0.29, # meter
    "Y": 0,
    "Z": 1.0,
    "YAW": 0.0, # deg
    "PITCH": 0.0,
    "ROLL": 0.0
}


params_visual = {
    "USE_INTENSITY_MAP": True,
    "INTENSITY_MAX": 255, # the maximum intensity shown on the intensity map
    "COLORMAP": cv2.COLORMAP_RAINBOW, # a type of color maps
    "DILATION_KERNAL": cv2.MORPH_ELLIPSE, # cv2.MORPH_RECT, cv2.MORPH_ELLIPSE, cv2.MORPH_CROSS
    "DILATION_SIZE": 5 # a type of color maps
}

def main():

    
    udp_cam = UDP_CAM_Parser(ip=params_cam["localIP"], port=params_cam["localPort"], params_cam=params_cam)

    udp_lidar = UDP_LIDAR_Parser(ip=params_lidar["localIP"], port=params_lidar["localPort"], params_lidar=params_lidar)

    l2c_trans = LIDAR2CAMTransform(params_cam, params_lidar)

    while True :

        # measurement
        if udp_cam.is_img==True and udp_lidar.is_lidar==True:
            t_s1 = time.time()
            img_cam = udp_cam.raw_img

            x= udp_lidar.x
            y=udp_lidar.y
            z=udp_lidar.z
            intensity=udp_lidar.Intensity
            
            xyz1 = np.concatenate([
                x.reshape([-1, 1]),
                y.reshape([-1, 1]),
                z.reshape([-1, 1]),
                np.ones_like(z.reshape([-1, 1]))
            ], axis=1).T.astype(np.float32)

            xyz = np.matmul(RotationMatrix(np.deg2rad(-90.), 0, 0).astype(np.float32), xyz1)

            xyz_p = xyz.T[:, :3]

            xyz_c = l2c_trans.transform_lidar2cam(xyz_p)

            intensity_c = intensity[np.where(xyz_c[:, 2]>=0)]

            xyz_c = xyz_c[np.where(xyz_c[:, 2]>=0)][:, :3]

            xy_i, intens_i = l2c_trans.project_pts2img(xyz_c, intens=intensity_c, crop=True)

            print(xy_i[:, 0].shape, intens_i.shape)

            img_l2c = make_intens_img(xy_i[:, 0].astype(np.int32),
                                    xy_i[:, 1].astype(np.int32),
                                    intens_i, params_cam["WIDTH"],
                                                            params_cam["HEIGHT"],
                                                            params_visual["INTENSITY_MAX"],
                                                            params_visual["COLORMAP"])

            img_cam = cv2.addWeighted(img_cam, 0.5, img_l2c, 0.5, 0.0)
        
            cv2.imshow("Lidar2Cam", img_cam)

            cv2.waitKey(1)

    
if __name__ == '__main__':

    main()