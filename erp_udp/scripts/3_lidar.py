import socket
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
from lidar_util import UDP_LIDAR_Parser


params_lidar = {
    "Range" : 90, #min & max range of lidar azimuths
    "CHANNEL" : 16, #verticla channel of a lidar
    "localIP": "127.0.0.1",
    "localPort": 2368,
    "Block_SIZE": int(1206)
}



def main():

    udp_lidar = UDP_LIDAR_Parser(ip=params_lidar["localIP"], port=params_lidar["localPort"], params_lidar=params_lidar)

    while True :

        if udp_lidar.is_lidar ==True:
            x=udp_lidar.x
            y=udp_lidar.y
            z=udp_lidar.z
            intensity=udp_lidar.Intensity 

            xyz1 = np.concatenate([
                x.reshape([-1, 1]),
                y.reshape([-1, 1]),
                z.reshape([-1, 1])
            ], axis=1).T.astype(np.float32)

            print(xyz1.T)
            # fig=plt.figure()
            # ax=fig.gca(projection='3d')
            # ax.scatter(x,y,z)
            # plt.show()






    
if __name__ == '__main__':

    main()