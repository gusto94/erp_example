import socket
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from cam_util import UDP_CAM_Parser



params_cam = {
    "localIP": "127.0.0.1",
    "localPort": 1232,
    "Block_SIZE": int(65000)
}


def main():

    udp_cam = UDP_CAM_Parser(ip=params_cam["localIP"], port=params_cam["localPort"], params_cam=params_cam)

    while True :


        if udp_cam.is_img==True :
            img_cam = udp_cam.raw_img

            cv2.imshow("cam", img_cam)

            cv2.waitKey(1)

    
if __name__ == '__main__':

    main()


